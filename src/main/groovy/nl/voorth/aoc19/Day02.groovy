package nl.voorth.aoc19

List<Integer> transform(List<Integer> input)
{
  p = 0
  while (input[p] != 99)
  {
    operand1 = input[p+1]
    operand2 = input[p+2]
    output = input[p+3]
    if (input[p] == 1) {input[output] = input[operand1] + input[operand2]}
    else if (input[p] == 2) {input[output] = input[operand1] * input[operand2]}
    p += 4
  }
  return input
}

List<Integer> set(List<Integer> input, int noun, int verb)
{
  result = input.clone()
  result[1] = noun
  result[2] = verb
  return result
}

assert transform([1,0,0,0,99]) == [2,0,0,0,99]
assert transform([2,3,0,3,99]) == [2,3,0,6,99]
assert transform([2,4,4,5,99,0]) == [2,4,4,5,99,9801]
assert transform([1,1,1,4,99,5,6,0,99]) == [30,1,1,4,2,5,6,0,99]

base = this.class.getResource('/day02.input').text.split(/,/).collect {it as int}

Map<Integer, Integer> results = [:]
for (int verb in 0..<100)
  for ( int noun in 0..<100)
  {
    println "($noun, $verb)"

    value =transform(set(base, noun, verb))[0]
    results[value] = 100*noun + verb
  }

println results[19690720]
