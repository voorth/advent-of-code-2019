package nl.voorth.aoc19

result = this.class.getResource('/day01.input')
  .readLines()
  .collect { it.toInteger()}
  .collect { it.intdiv(3) -2}
  .sum()
assert result == 3497998

int fuelFor( mass )
  {
    fuel = mass / 3 -2
    if (fuel <= 0) return 0
    return fuel + fuelFor(fuel)
  }

assert fuelFor(14) == 2
assert fuelFor(1969) == 966
assert fuelFor(100756) == 50346

result = this.class.getResource('/day01.input')
  .readLines()
  .collect { it.toInteger()}
  .collect { fuelFor it}
  .sum()
assert result == 5244112
