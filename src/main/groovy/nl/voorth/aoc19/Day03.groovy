package nl.voorth.aoc19

import java.awt.Point

List<Point> segment (Point start, String direction)
{
  switch (direction.substring(0, 1)) {
    case 'R' :
      action = { Point p, int d -> translate p, d,0 }
      break
    case 'L':
      action = { Point p, int d -> translate p, -d,0 }
      break
    case 'U':
      action = { Point p, int d -> translate p, 0,d }
      break
    case 'D':
      action = { Point p, int d -> translate p, 0,-d }
      break
  }
  distance = direction.substring(1) as int
  (1..distance).collect { action(start, it)}
}

Point translate(Point p, int dx, int dy)
{
  result = new Point(p)
  result.translate(dx, dy)
  result
}

List<Point> pathFor(String line)
{
  directions = line.split(/,/)
  point = new Point(0, 0)
  List<Point> path = []
  for (dir in directions) {
    segment = segment(point, dir)
    point = segment[-1]
    path.addAll(segment)
  }
  path
}

assert nearestCrossing('R8,U5,L5,D3', 'U7,R6,D4,L4') == 6
assert nearestCrossing( 'R75,D30,R83,U83,L12,D49,R71,U7,L72', 'U62,R66,U55,R34,D71,R55,D58,R83') == 159
assert nearestCrossing( 'R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51', 'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7') == 135

paths = this.class.getResource('/day03.input').readLines()
assert paths.size() == 2
println nearestCrossing(paths[0], paths[1])

int nearestCrossing(String line1, String line2)
{
  pathFor(line1).intersect(pathFor(line2))
    .collect { Math.abs(it.x) + Math.abs(it.y) }
    .min() as int
}

